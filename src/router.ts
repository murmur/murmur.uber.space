import * as Router from 'koa-router';
import { indexPage, create } from './routes/create';
import { showOrCreateInstance } from './routes/instance';
import { metrics } from './routes/metrics';

const router = new Router();
router.get('/', indexPage);
router.post('/create', create);
router.get('/admin/:uuid', showOrCreateInstance);
router.get('/metrics', metrics)
export default router;