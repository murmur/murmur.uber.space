import {Entity, Column, PrimaryColumn} from "typeorm";

@Entity()
export class Instance {

    @PrimaryColumn()
    uuid: string;

    @Column()
    state: 'new'|'creating'|'running'|'deleted'

    @Column({nullable: true})
    host?: string = '';

    @Column({nullable: true})
    port?: number;

    @Column()
    serverPassword: string

    @Column()
    suPassword: string

    @Column()
    createdAt: Date

    @Column()
    validUntil: Date


}

