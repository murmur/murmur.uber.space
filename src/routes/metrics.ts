import * as Koa from 'koa';
import { HOSTS } from '../config'
import { Instance }  from '../entity/instance'
import { getRepository  } from 'typeorm';
import { sshCommand } from '../commands';

export async function metrics(ctx: Koa.Context) {
    let out = ''
    const repo = getRepository(Instance)
    out += '# HELP murmur_connections Currently open mumble connections\n'
    out += '# TYPE murmur_connections gauge\n'
    for(let host of HOSTS) {
        console.log(host)
        const res = await sshCommand(host.sshString, ['/usr/sbin/ss', '-utp', '|', 'grep', 'murmur', '|', 'wc', '-l'])
        out += `murmur_connections{host="${host.hostname}"} ${parseInt(res.stdout.toString().trim())}\n`
    }
    out += '\n'
    out += '# HELP murmur_instances Running murmur instances\n'
    out += '# TYPE murmur_instances gauge\n'
    for(let host of HOSTS) {
        const instances = await repo.find({ host: host.hostname, state: 'running' })
        out += `murmur_instances{host="${host.hostname}"} ${instances.length}\n`
    }
    ctx.body = out
}