import * as Koa from 'koa';
import { BASE_URL, TIME_FRAMES } from '../config'
import { Instance }  from '../entity/instance'
import { getRepository  } from 'typeorm';
import { randomWords, sendmail } from '../utils';
import * as crypto from 'crypto'
import { v4 as uuid } from 'uuid'
import { promises as fs } from 'fs'
import * as path from 'path'
import * as svgCaptcha from 'svg-captcha'

const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/


const captches_solutions: {[token: string]: string} = {}
function getCaptcha() {
    const captcha = svgCaptcha.create({ size: 6, noise: 10 })
    const token = crypto.randomBytes(20).toString('hex')
    captches_solutions[token] = captcha.text

    setTimeout( () => {
        if(!captches_solutions[token]) return
        delete captches_solutions[token]
    }, 10*60*1000)

    return {
        image: captcha.data,
        token: token
    }
}

export async function indexPage(ctx:Koa.Context) {
    const captcha = getCaptcha()
    await ctx.render("index", {
        time_frames: TIME_FRAMES,
        password: randomWords(4),
        token: captcha.token,
        captcha: captcha.image
    })
}


const longestDuration = TIME_FRAMES.map(a => a.duration).sort()[0]
export async function create(ctx:Koa.Context) {
    let errors = []
    const body = ctx.request.body as any
    if(!body.email) errors.push('email is missing')
    else if(typeof body.email !== 'string' || !body.email.match(EMAIL_REGEX)) errors.push('invalid mail')

    if(typeof body.password !== 'string') errors.push('server password is invalid')
    else if(!body.password.trim()) errors.push('server password is missing')

    if(typeof body.duration !== 'string') errors.push('invalid duration')
    else if(parseInt(body.duration) < 300 || parseInt(body.duration) > longestDuration) errors.push('invalid duration')

    if(!body.captcha || typeof body.captcha !== 'string' || !body.token || typeof body.token !== 'string') errors.push('captcha wrong. try again')
    else {
        const solution = captches_solutions[body.token]
        if(body.captcha.trim() !== solution) errors.push('captcha wrong. try again')
    }

    if(errors.length) {
        const captcha = getCaptcha()
        await ctx.render("index", {
            time_frames: TIME_FRAMES,
            errors: errors,
            email: body.email,
            password: body.password,
            token: captcha.token,
            captcha: captcha.image
        })
        return
    } else {
        delete captches_solutions[body.token]
        const email = body.email.trim()
        const password = body.password.trim()
        const duration = parseInt(body.duration)
        const instance = new Instance
        instance.createdAt = new Date
        instance.state = 'new'
        instance.uuid = uuid()
        instance.serverPassword = password
        instance.suPassword = crypto.randomBytes(20).toString('base64').slice(0,20)
        instance.validUntil = new Date(Date.now() + duration*1000)
        await getRepository(Instance).save(instance)
        const url = `${BASE_URL}/admin/${instance.uuid}`

        let mailbody = await fs.readFile(path.join(__dirname, '../mail.txt'), 'utf-8')
        mailbody = mailbody.replace(/\[URL\]/g, url)
        mailbody = mailbody.replace(/\[EXPIRATION_DATE\]/g, instance.validUntil.toISOString().slice(0,16).replace('T', ' ')+' UTC')
        await sendmail(email, 'Your new mumble server 🎉', mailbody)
        await ctx.render("index-success")
    }
}
