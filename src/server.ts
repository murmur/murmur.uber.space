import "reflect-metadata"; 
import {createConnection} from "typeorm";
import * as Koa from 'koa';
import * as bodyParser  from 'koa-bodyparser'
import router from './router'
import * as logger from 'koa-logger'
import koaTwig from './koa-twig'
import * as serve from 'koa-static'
import * as path from 'path'
const app  = new Koa();

app.use(logger())
app.use(serve(path.join(__dirname, '../public')))
app.use(bodyParser())
app.use(
    koaTwig({
      views: `${__dirname}/views`,
      extension: "twig",
    //   errors: { 404: "not-found" }, // A 404 status code will render the file named `not-found`
      data: { NODE_ENV: process.env.NODE_ENV }, // Data shared accross all views
    })
);

app.use(router.routes())

// Application error logging.
app.on('error', console.error);

const PORT = Number(process.env.PORT) || 3000;

async function start() {
    await createConnection()
    console.log('database connected')
    app.listen(PORT, () => {
        console.log('serving on port '+PORT)
    });
}
start()