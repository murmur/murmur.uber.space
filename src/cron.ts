import { HOSTS } from './config'
import * as https from 'https'
import { removeInstance, sshCommand } from './commands'
import { createConnection, getConnection, getRepository, Not, Raw } from 'typeorm'
import { Instance } from './entity/instance'

async function main() {
    await createConnection()
    const repo = getRepository(Instance)

    console.log('maybe trigger HTTP certificate refresh')
    for(let host of HOSTS) {
        await new Promise( (resolve: any) => {
            const req = https.request({
                hostname: host.hostname
            }, (res) => {
                console.log(host.hostname, res.statusCode)
                resolve()
            })
            req.end()
        })
    }
    const isSqlite = getConnection().options.type == 'sqlite'
    const instances = await repo.find({
        state: "running",
        validUntil: Raw((alias) => isSqlite ? `${alias} < strftime('%Y-%m-%d %H:%M:%f','now')` : `${alias} < NOW()`)
    })
    for(let instance of instances) {
        console.log('remove '+instance.uuid)
        try {
            const host = HOSTS.find(h => h.hostname == instance.host)
            await removeInstance(host.sshString, instance.uuid, instance.port)
            instance.state = 'deleted'
            await repo.save(instance)
        } catch(err) {
            console.error(err)
        }
    }
    getConnection().close()
}
main()