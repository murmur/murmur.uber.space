import { twig, renderFile as renderFileSync } from 'twig'
import * as Koa from 'koa';
import * as util from 'util'
const renderFile: any = util.promisify(renderFileSync);


export default function (config: any) {
  if (!config.views) {
    throw new Error("`views` is required in config");
  }

  const extension = config.extension || "twig";
  const defaultData = config.data || {};

 return async (ctx: Koa.Context, next: any) => {
    /**
     * Render a twig template
     * @param {string} view
     * @param {object} data
     */
    async function render(view: string, data = {}) {
        if (!view) {
        throw new Error("`view` is required in render");
        }

        const viewPath = `${config.views}/${view}.${extension}`;

        ctx.type = "text/html";
        ctx.body = await renderFile(viewPath, {
            ...defaultData,
            ...data,
        });
    }

    (ctx.response as any).render = render;
    ctx.render = render;
    await next()
 }
};

