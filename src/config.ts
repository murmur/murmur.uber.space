export const BASE_URL = 'https://murmur.uber.space'
export const TIME_FRAMES = [
    {
        duration: 6*3600,
        label: '6 hours'
    },
    {
        duration: 24*3600,
        label: '1 day'
    },
    {
        duration: 3*86400,
        label: '3 days'
    },
    {
        duration: 7*86400,
        label: '7 days'
    },
    {
        duration: 14*86400,
        label: '14 days'
    },
]

interface Host {
    sshString: string
    hostname: string
}
export const HOSTS: Host[] = [
    {
        sshString: 'murmur@auriga.uberspace.de',
        hostname: 'murmur.uber.space'
    }
]

export const MAIL_CONFIG = {
    host: "auriga.uberspace.de",
    port: 465,
    secure: true,
    auth: {
        user: 'murmur-no-reply-out',
        pass: process.env.MURMUR_SMTP_PASS
    }
}
export const MAIL_FROM = 'no-reply@murmur.uber.space'