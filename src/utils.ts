import { HOSTS } from './config'
import * as fs from 'fs'
import * as path from 'path'
import * as crypto from 'crypto'
import * as nodemailer from 'nodemailer'
import { MAIL_CONFIG, MAIL_FROM } from './config'

export async function getLeastLoadedHost() {
    if(HOSTS.length == 1) return HOSTS[0]
    throw new Error('getLeastLoadedHost() is not implemented yet')
}

const wordlist = fs.readFileSync(path.join(__dirname, 'wordlist'), 'utf-8').trim().split('\n')

export function randomWords(length: number) {
    let words = ''
    for(let i=0;i<length;i++) {
        const word = wordlist[crypto.randomInt(0, 2048)]
        words += word[0].toLocaleUpperCase()
        words += word.slice(1)
    }
    return words
}

const transport = nodemailer.createTransport(MAIL_CONFIG)

export async function sendmail(to: string, subject: string, body: string) {   
    await transport.sendMail({
        from: MAIL_FROM,
        to,
        subject,
        text: body
    })
}
